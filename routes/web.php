<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->options('/{any:.*}', ['middleware' => ['CorsMiddleware'], function () {
    return response(['status' => 'success']);
}]);

$router->group(['middleware' => 'CorsMiddleware'], function () use ($router) {
    $router->get('/', function () use ($router) {
        return $router->app->version();
    });

    // Authentication
    $router->post('/login', 'AuthController@login');

    // JWT Authenticated Middleware
    $router->group(['middleware' => 'jwt.auth'], function () use ($router) {
        
        // Product Resource Routes
        $router->get('manage/products', 'ProductController@index');
        $router->post('manage/products', 'ProductController@store');
        $router->put('manage/products/{id}', 'ProductController@update');
        $router->delete('manage/products/{id}', 'ProductController@destroy');

        // Users Resource Routes
        $router->get('manage/users', 'UserController@index');
        $router->post('manage/users', 'UserController@store');
        $router->put('manage/users/{id}', 'UserController@update');
        $router->delete('manage/users/{id}', 'UserController@destroy');

        // Product Listed Routes
        $router->get('/lists', 'ProductListController@getListedProducts');
        $router->get('/lists/not', 'ProductListController@getUnlistedProducts');
        $router->post('/lists/add', 'ProductListController@addToList');
        $router->post('/lists/remove', 'ProductListController@removeFromList');
    });
});

