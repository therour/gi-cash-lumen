<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\GeneratorCommand;

class MakeRepositoryCommand extends GeneratorCommand
{
	/**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {name} {--I|interface}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';
    
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/repository.stub';
    }
    
    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Repositories';
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceNamespace($stub, $name)->replaceImplement($stub, $name)->replaceClass($stub, $name);
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        $class = str_replace($this->getNamespace($name).'\\', '', $name);

        return str_replace('DummyClass', $class, $stub);
    }


    protected function replaceImplement(&$stub, $name)
    {
    	$interface = str_replace($this->getNamespace($name).'\\', '', $name.'Interface');
    	$stub = str_replace(
    		'DummyRepositoryInterface',
    		$interface,
    		$stub
    	);

    	return $this;
    }


    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $name = $this->qualifyClass($this->getNameInput())."Repository";

        $path = $this->getPath($name);

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        if ((! $this->hasOption('force') ||
             ! $this->option('force')) &&
             $this->alreadyExists($this->getNameInput())) {
            $this->error($this->type.' already exists!');

            return false;
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');

        $this->info($this->option('interface'));
        // Create Repository Interface
        if ($this->hasOption('interface') && $this->option('interface')) {

        	$interfaceName = $this->qualifyInterface($this->getNameInput())."RepositoryInterface";
        	
	        $interfacePath = $this->getPath($interfaceName);

	        $this->makeDirectory($interfacePath);

	        $this->files->put($interfacePath, $this->buildInterface($interfaceName));

	        $this->info('Also it\'s interface has created successfully.');
        }
    }

    protected function qualifyInterface($name)
    {
        $name = ltrim($name, '\\/');

        $rootNamespace = $this->rootNamespace();

        if (Str::startsWith($name, $rootNamespace)) {
            return $name;
        }

        $name = str_replace('/', '\\', $name);

        return $this->qualifyClass(
            $this->getInterfaceNamespace(trim($rootNamespace, '\\')).'\\'.$name
        );
    }

    protected function getInterfaceNamespace($rootNamespace)
    {
    	return $rootNamespace.'\Repositories\Interfaces';
    }

    protected function buildInterface($name)
    {
    	$stub = $this->files->get($this->getInterfaceStub());

    	return $this->replaceNamespace($stub, $name)->replaceInterface($stub, $name);
    }

    protected function replaceInterface($stub, $name)
    {
    	$interface = str_replace($this->getNamespace($name).'\\', '', $name);

    	return str_replace('DummyRepositoryInterface', $interface, $stub);
    }

    protected function getInterfaceStub()
    {
    	return __DIR__.'/stubs/repositoryInterface.stub';
    }
}