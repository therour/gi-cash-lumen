<?php

namespace App\Repositories;

use App\Models\Shop;

class ShopRepository extends Repository implements Interfaces\ShopRepositoryInterface
{
    protected function model()
    {
    	return Shop::class;
    }

    public function getByUser($userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }
}