<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends Repository implements Interfaces\UserRepositoryInterface
{
	protected function model()
	{
		return User::class;
	}

	public function findByUsername($username)
	{
		return $this->model->where('username', $username)->first();
	}
}