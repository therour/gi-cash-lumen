<?php

namespace App\Repositories;

abstract class Repository implements Interfaces\RepositoryInterface
{
	private $db;

	protected $model;

	public function __construct()
	{
		$this->db = app('db');

		$this->model = app($this->model());
	}

	abstract protected function model();

	public function all()
	{
		return $this->model->all();
	}

	public function find($id)
	{
		return $this->model->find($id);
	}

	public function create($data)
	{
		return $this->model->create($data);
	}

	public function update($id, $data)
	{
		return $this->model->find($id)->update($data);
	}

	public function delete($id)
	{
		return $this->model->destroy($id);
	}
}