<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends Repository implements Interfaces\ProductRepositoryInterface
{
    protected function model()
    {
    	return Product::class;
    }

    public function getNotIn($ids)
    {
        if (! is_array($ids)) {
            $ids = array($ids);
        }
        return $this->model->whereNotIn('id', $ids)->get();
    }
}