<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{
    protected $request;
    
    protected $product;

    public function __construct(Request $request, ProductRepository $product)
    {
        $this->request = $request;
        $this->product = $product;    
    }

    public function index()
    {
        return $this->product->all();
    }

    public function store()
    {
        $this->validate($this->request, [
            'name' => 'required|string|min:5',
            'stock' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:5000',
            'description' => 'required|string'
        ]);

        $product = $this->product->create($this->request->all());

        return response()->json([
            'status' => 'success',
            'product' => $product
        ], 201);
    }

    public function update($id)
    {
        $this->validate($this->request, [
            'name' => 'required|string|min:5',
            'stock' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:5000',
            'description' => 'required|string'
        ]);

        $product = $this->product->update($id, $this->request->all());

        return response()->json([
            'status' => 'updated'
        ]);
    }

    public function destroy($id)
    {
        $this->product->delete($id);

        return response()->json([
            'status' => 'deleted'
        ]);
    }
}