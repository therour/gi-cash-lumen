<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Repositories\UserRepository;
use Validator;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class AuthController extends Controller
{
    private $request;

    private $userRepo;

    public function __construct(Request $request, UserRepository $userRepo)
    {
        $this->request = $request;

        $this->userRepo = $userRepo;
    }

    protected function jwt($userId)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer the token
            'sub' => $userId, // Subect of the token
            'iat' => time(), // Time when JWT was issued
            'exp' => time() + 60 * 60 // Expiration time
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function login()
    {
        $this->validate($this->request,[
            'username' => 'required',
            'password' => 'required',
        ]);

        $user = $this->userRepo->findByUsername($this->request->input('username'));

        if (! $user) {
            return response()->json([
                'username' => [
                    'Username or Password is incorrect.'
                ]
            ], 403);
        }

        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user->id),
                'user' => $user,
            ], 200);
        }

        return response()->json([
            'username' => [
                'Username or Password is incorrect.'
            ]
        ], 403);
    }
}