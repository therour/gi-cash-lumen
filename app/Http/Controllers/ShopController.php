<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ShopRepository;

class ShopController extends Controllers
{
    protected $request;

    protected $shopRepo;

    public function __construct(Request $request, ShopRepository $shopRepo)
    {
        $this->request = $request;

        $this->shopRepo = $shopRepo;
    }

    public function index()
    {
        return $this->shopRepo->all();
    }
}