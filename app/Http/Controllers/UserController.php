<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $request;
    
    protected $user;

    public function __construct(Request $request, UserRepository $user)
    {
        $this->request = $request;
        $this->user = $user;    
    }

    public function index()
    {
        return $this->user->all();
    }

    public function store()
    {
        $this->validate($this->request, [
            'name' => 'required|string|min:5',
            'username' => 'required|string|min:4',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required'
        ]);
        
        $this->request->merge(['password' => app('hash')->make($this->request->password)]);

        $user = $this->user->create($this->request->all());

        return response()->json([
            'status' => 'success',
            'user' => $user
        ], 201);
    }

    public function update($id)
    {
        $this->validate($this->request, [
            'name' => 'required|string|min:5',
            'username' => 'required|string|min:4',
            'role' => 'required'
        ]);

        $user = $this->user->update($id, $this->request->except('password'));

        return response()->json([
            'status' => 'updated'
        ]);
    }

    public function destroy($id)
    {
        $this->user->delete($id);

        return response()->json([
            'status' => 'deleted'
        ]);
    }
}