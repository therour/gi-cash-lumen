<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class ProductListController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getListedProducts()
    {
        return $this->request->auth->productsListed;
    }

    public function getUnlistedProducts(ProductRepository $product)
    {
        $productSelected = $this->request->auth->productsListed->pluck('id')->all();

        return $product->getNotIn($productSelected);
    }

    public function addToList()
    {
        var_dump($this->request->product_ids);
        $this->request->auth->productsListed()->attach($this->request->product_ids);
    }

    public function removeFromList()
    {
        $this->request->auth->productsListed()->detach($this->request->product_ids);
    }
}