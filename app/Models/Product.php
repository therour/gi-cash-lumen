<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attribtue that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'stock', 'description'
    ];
    
    /**
     * The relationship of many to many between users and products.
     */
    public function usersListing()
    {
        return $this->belongsToMany(User::class, 'lists', 'product_id', 'user_id');
    }
}
