<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'username' => $faker->unique()->firstName,
        'password' => app('hash')->make('secret'),
        'role' => 'owner'
    ];
});

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'price' => $faker->numberBetween(10, 1000) * 1000,
        'stock' => $faker->numberBetween(5, 100),
    ];
});
