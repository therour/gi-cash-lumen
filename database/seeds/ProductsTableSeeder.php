<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run() 
    {
        factory(App\Models\Product::class, 100)->create();
    }
}
